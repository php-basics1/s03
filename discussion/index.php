<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP Classes and Objects</title>
</head>
<body>
    
    <h1>Classes and Objects</h1>
    
    <h2>Object as Variables</h2>
    <pre><?php var_dump($buildingObj); ?></pre>

    <p>Building Name: <?= $buildingObj->name;?></p>
    <p>Building Name: <?= $buildingObj->floors;?></p>
    <p>Building Name: <?= $buildingObj->address->city;?></p>
    <p>Building Name: <?= $buildingObj->address->country;?></p>


    <h2>Objects from Classes</h2>
    
    <pre><?php var_dump($building); ?></pre>


    <!-- Accessing the printName() method of the instantiated object. -->
    <p>printName() method: <?= $building->printName(); ?></p>


    <h1>Inheritance and Polymorphism</h1>

    <h2>Inheritance (Condominium Object)</h2>
    <pre><?php var_dump($condominium) ?></pre>

    <p>Condominium name: <?= $condominium->name; ?></p>
    <p>Condominium floors: <?= $condominium->floor; ?></p>
    

    <h2>Polymorphism (Overriding the behaviour of the printName() method)</h2>
    <p>printName() Method for Condominium: <?= $condominium->printName(); ?></p>



</body>
</html>